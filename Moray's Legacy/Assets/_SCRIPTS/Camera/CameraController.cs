﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CameraController {

    [SerializeField]
    private CameraModel model;

    public float GetMouseSensitivity()
    {
        return model.mouseSensitivity;
    }
    
}
