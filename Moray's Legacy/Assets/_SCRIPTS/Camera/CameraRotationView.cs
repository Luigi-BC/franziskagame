﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotationView : ViewBase<CameraController>{

    private Vector2 _mouseInput;
    private float yRotation;
    private float xRotation;


    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        _mouseInput = GetMouseInput();

        yRotation += (_mouseInput.x * Controller.GetMouseSensitivity() * Time.deltaTime);
        xRotation -= (_mouseInput.y * Controller.GetMouseSensitivity() * Time.deltaTime);

        xRotation = Mathf.Clamp(xRotation, -80f, 80f);

        transform.rotation = Quaternion.Euler(new Vector3(xRotation, yRotation, 0f));
    }

    private Vector2 GetMouseInput()
    {
        Vector2 mouseInput;
        mouseInput.x = Input.GetAxis("Mouse X");
        mouseInput.y = Input.GetAxis("Mouse Y");

        return mouseInput;
    }
}
