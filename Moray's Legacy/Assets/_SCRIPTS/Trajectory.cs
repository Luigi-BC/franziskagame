﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Trajectory
{
    [SerializeField]
    private float horizontalDistance;
    [SerializeField]
    private float velocityAtStart;
    [SerializeField]
    private Vector3 posAtStart;
    [SerializeField]
    private Vector3 posAtEnd;
    [SerializeField]
    private float angle;
    [SerializeField]
    public float airTime;
    [SerializeField]
    private float gravity;
    [SerializeField]
    private Vector3 direction;
    private float cConstant;
    private float startingYOffset;


    

    public void SetTrajectoryValues(float startingAngle, Vector3 startingPoint, Vector3 endPoint, float gravityValue = 9.81f)
    {
        //get values
        angle = Mathf.Deg2Rad * startingAngle;
        posAtStart = startingPoint;
        posAtEnd = ValidateEndPoint(endPoint);
        //posAtEnd = endPoint;
        gravity = gravityValue;

        //calculate new Values
        Vector3 posStartGrounded = GroundVector(posAtStart);
        Vector3 posEndGrounded = GroundVector(posAtEnd);
        direction = posEndGrounded - posStartGrounded;
        horizontalDistance = direction.magnitude;

        //startingYOffset
        startingYOffset = posAtStart.y - posAtEnd.y;

        velocityAtStart = CalculateStartingVelocity();

        cConstant = (2 * startingYOffset * gravity) / Mathf.Pow(velocityAtStart,2);

        airTime = CalculateAirTime();
    }

    private Vector3 ValidateEndPoint(Vector3 endPoint)
    {
        Vector3 validEndPoint = endPoint;

        float maxHeight = (horizontalDistance / Mathf.Sin(Mathf.Deg2Rad * (180f - 90f - (Mathf.Rad2Deg* angle))) * Mathf.Sin(angle));

        maxHeight += posAtStart.y;

        if(endPoint.y >= maxHeight)
        {
            validEndPoint.y = maxHeight - 0.01f;
            Debug.Log("OYOY");
        }
        return validEndPoint;
    }

    private float CalculateXPosition(float time) 
    {
        Vector3 directionXY = direction.normalized * velocityAtStart;
        directionXY = new Vector3(directionXY.x, directionXY.y, 0f);

        float velXY = directionXY.magnitude * Mathf.Sign(direction.x);

        return (velXY * Mathf.Cos(angle) * time);
    }

    private float CalculateZPosition(float time)
    {
        Vector3 directionYZ = direction.normalized * velocityAtStart;

        directionYZ = new Vector3(0f, directionYZ.y, directionYZ.z);

        float velYZ = directionYZ.magnitude * Mathf.Sign(direction.z);
        
        return (velYZ * Mathf.Cos(angle) * time);
    }
    
    public Vector3 CalculatePosition(float _time)
    {
        Vector3 pos;
        pos.x = CalculateXPosition(_time);
        pos.y = CalculateYPosition(_time);
        pos.z = CalculateZPosition(_time);
        pos += posAtStart;
        return pos;
    }

    private float CalculateYPosition(float time)
    {
        return (-0.5f * gravity * (time * time) + (velocityAtStart * Mathf.Sin(angle) * time ));
        //return (-0.5f * gravity * (time* time) + (velocityAtStart * Mathf.Sin(angle) * time + posAtStart.y));
    }

    private float CalculateStartingVelocity()
    {
        return (horizontalDistance / Mathf.Cos(angle) * Mathf.Sqrt(gravity / (2 * (horizontalDistance * Mathf.Tan(angle) + startingYOffset))));
    }

    private float CalculateAirTime()
    {
        return (velocityAtStart / gravity) * (Mathf.Sin(angle) + Mathf.Sqrt(Mathf.Sin(angle) * Mathf.Sin(angle) + cConstant));
    }


    public Vector3 GroundVector(Vector3 inputVector)
    {
        return new Vector3(inputVector.x, 0f, inputVector.z);
    }   
}
