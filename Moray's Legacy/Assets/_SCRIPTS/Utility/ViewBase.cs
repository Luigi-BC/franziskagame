﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewBase<ControllerType> : MonoBehaviour where ControllerType : class {

    [SerializeField]
    private ControllerType _controller;
    public ControllerType Controller
    {
        get
        {
            return _controller;
        }
        set
        {
            _controller = value;
        }
    }

    protected void OnControllerChanged(ControllerType oldController, ControllerType newController)
    {

    }

    protected void Refresh()
    {

    }
}
