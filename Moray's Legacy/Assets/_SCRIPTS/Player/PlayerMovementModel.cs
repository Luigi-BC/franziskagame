﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerMovementModel
{

    public float walkSpeed;
    public float runSpeed;
    public float jumpForce;
    public float additionalGravity;
    


}

