﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerModelRotation : ViewBase<PlayerMovementView>
{

    [SerializeField]
    [Range(0,0.5f)]
    private float _rotationLerpValue;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {

        Vector3 rotationVector = Controller._desiredVelocity;
        rotationVector.y = 0f;
        if(rotationVector.magnitude < 0.3f)
        {
            return;
        }
        rotationVector = rotationVector.normalized;
        Quaternion newRotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(rotationVector), _rotationLerpValue);

        transform.rotation = newRotation;
	}
}
