﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerMovementController
{

    [SerializeField]
    private PlayerMovementModel model;

    private float _currentMovementSpeed;
    private PlayerMovementState _currentMovementState;
    public PlayerMovementState CurrentMovementState
    {
        get { return _currentMovementState; }
        set
        {
            _currentMovementState = value;
        }
    }

    public float MovementSpeed
    {
        get
        {
            switch (_currentMovementState)
            {
                case PlayerMovementState.Walking:
                    return model.walkSpeed;
                case PlayerMovementState.Running:
                    return model.runSpeed;
            }
            return model.walkSpeed;
        }
    }

    public float JumpForce
    {
        get { return model.jumpForce; }
    }

    public float AdditionalGravity
    {
        get { return model.additionalGravity; }
    }


}


public enum PlayerMovementState { Walking, Running };