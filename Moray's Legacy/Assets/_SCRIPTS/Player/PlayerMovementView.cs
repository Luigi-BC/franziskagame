﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovementView : ViewBase<PlayerMovementController>
{

    [SerializeField]
    private Transform _cameraRotationCenter;

    private Rigidbody _rigidbody;
    private Vector2 _rawInputVector;

    [HideInInspector]
    public Vector3 _desiredVelocity;

    [SerializeField]
    private LayerMask _groundCheckLayerMask;

    [SerializeField]
    private bool _grounded;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        
    }

    private void Update ()
    {
        _rawInputVector = GetNormalizedMovementInput();
        _grounded = GroundCheck();

        //handle sprint
        if (GetSprintInput())
        {
            Controller.CurrentMovementState = PlayerMovementState.Running;
        }
        else
        {
            Controller.CurrentMovementState = PlayerMovementState.Walking;
        }

        //Handle Jump
        if (GetJumpInput() == true)
        {
            if(_grounded == true)
            {
                Jump();
            }
        }
	}


    private bool GroundCheck()
    {
        return Physics.CheckSphere(transform.position, 0.1f, _groundCheckLayerMask);
    }

    private void Jump()
    {
        Vector3 temp = _rigidbody.velocity;
        temp.y = Controller.JumpForce;
        _rigidbody.velocity = temp;
    }


    private void FixedUpdate()
    {
        //Set up movement
        _desiredVelocity = new Vector3(_rawInputVector.x, 0f, _rawInputVector.y) * Time.fixedDeltaTime * Controller.MovementSpeed;
        _desiredVelocity.y = _rigidbody.velocity.y + Controller.AdditionalGravity;

        //rotate accordingly to camera rotation
        Quaternion flattenedRotation = Quaternion.Euler(new Vector3(0f, _cameraRotationCenter.rotation.eulerAngles.y, 0f));
        _desiredVelocity = flattenedRotation * _desiredVelocity;


        _rigidbody.velocity = _desiredVelocity;
    }

    private bool GetJumpInput()
    {
        return Input.GetKeyDown(KeyCode.Space);
    }

    private Vector2 GetNormalizedMovementInput()
    {
        Vector2 inputVector;
        inputVector.x = Input.GetAxisRaw("Horizontal");
        inputVector.y = Input.GetAxisRaw("Vertical");
        inputVector.Normalize();

        return inputVector;
    }

    private bool GetSprintInput()
    {
        return Input.GetKey(KeyCode.LeftShift);
    }

}
