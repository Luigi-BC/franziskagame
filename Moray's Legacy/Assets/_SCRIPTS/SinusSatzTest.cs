﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinusSatzTest : MonoBehaviour {

    [SerializeField]
    private float xDistance;
    [SerializeField]
    private float angle;
    [SerializeField]
    private float hDistance;
    [SerializeField]

	
	// Update is called once per frame
	void Update ()
    {
        CalculateHeight();
	}

    private void CalculateHeight()
    {
        hDistance = (xDistance / Mathf.Sin(Mathf.Deg2Rad * (180f - 90f - angle)) * Mathf.Sin(Mathf.Deg2Rad * angle));
    }
}
