﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Throwable : MonoBehaviour {

    public Trajectory _trajectory;


    public void SetTrajectory(Trajectory inputTraj)
    {
        Debug.Log("settingInpu");
        _trajectory = inputTraj;
    }
	
    public IEnumerator ThrowObject()
    {
        float _timePassed = 0f;

        while(_timePassed < 5f)
        {

            yield return new WaitForEndOfFrame();
            transform.position = _trajectory.CalculatePosition(_timePassed);
            _timePassed += Time.deltaTime;
        }
        yield return null;
    }
	
}
