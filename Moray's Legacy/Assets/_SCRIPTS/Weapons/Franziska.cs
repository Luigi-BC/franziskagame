﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Franziska : MonoBehaviour {

    [SerializeField]
    private Transform _startingPoint;
    [SerializeField]
    private Transform _targetPoint;

    [SerializeField]
    private float _throwAngle;

    [SerializeField]
    private Trajectory trajectory;

    [SerializeField]
    private LineRenderer _lineRenderer;
    [SerializeField]
    private int _resolution;

    [SerializeField]
    private Transform _throwablePrefab;
    [SerializeField]
    private Transform _camera;

    [SerializeField]
    private LayerMask _hitLayer;

    private void Awake()
    {
        trajectory = new Trajectory();
        _lineRenderer = GetComponent<LineRenderer>();
    }



    private void Update()
    {

        Vector3 targetPoint = GetTargetPoint();

        //Axe
        //_throwAngle = _camera.rotation.eulerAngles.x + 20f;
        Vector3 _cameraDirectForward = Quaternion.Euler(0f, _camera.rotation.eulerAngles.y, 0f) * Vector3.forward;
        _throwAngle = Vector3.SignedAngle (_camera.forward, _cameraDirectForward, _camera.right);
        Debug.Log(_throwAngle);
        _throwAngle += 20f;
        _throwAngle = Mathf.Min(_throwAngle, 85f);
        trajectory.SetTrajectoryValues(_throwAngle, transform.position, targetPoint);


        //Debug
        //trajectory.SetTrajectoryValues(_throwAngle, _startingPoint.position, _targetPoint.position);
        _lineRenderer.positionCount = _resolution;
        _lineRenderer.SetPositions(CalculateAllPoints());

        if (Input.GetMouseButtonDown(0))
        {
            //THROW OBJECT 
            Transform currentObject = Instantiate(_throwablePrefab, transform.position, Quaternion.identity, null);
            Throwable throwableScript = currentObject.GetComponent<Throwable>();
            throwableScript._trajectory.SetTrajectoryValues(_throwAngle, transform.position, targetPoint);
            throwableScript.StartCoroutine("ThrowObject");
        }

    }

    private Vector3 GetTargetPoint()
    {
       
        RaycastHit hitInfo;
        float maxThrowDistance = 20f;

        if(Physics.Raycast(_camera.position, _camera.forward, out hitInfo, maxThrowDistance, _hitLayer))
        {
            return hitInfo.point;
        }
        else
        {
            return _camera.position + (_camera.forward.normalized * maxThrowDistance);
        }

    }
    
    private Vector3[] CalculateAllPoints()
    {
        Vector3[] points = new Vector3[_resolution];
        float airTime = trajectory.airTime;

        for (int i = 0; i < points.Length; i++)
        {
            points[i] = trajectory.CalculatePosition((float)i / (float)(_resolution -1) * airTime);
        }

        points[points.Length - 1] = trajectory.CalculatePosition(airTime);
        return points;
    }

}
